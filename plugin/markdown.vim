" Disable Folding
let g:vim_markdown_folding_disabled = 1
let g:vim_markdown_emphasis_multiline = 0
" To enable conceal use Vim's standard conceal configuration
set conceallevel=2
let g:vim_markdown_frontmatter = 1
let g:vim_markdown_json_frontmatter = 1
let g:vim_markdown_new_list_item_indent = 2
let g:vim_markdown_conceal_code_blocks = 0
